# Multi-Lang Demo

### Main Multi-Language Support Library:

-   [GitHub](https://github.com/i18next/i18next)
-   [Docs](https://www.i18next.com/)

### Inspired by Article

[How to offer multi-language support in a react-native app](https://www.crowdbotics.com/blog/how-to-offer-multi-language-support-in-a-react-native-app)

### To run

-   `yarn install`
-   `yarn start` (to launch expo)
-   follow instructions from expo

⚠️ May need to install **Expo Go** application on device

### Screen Recording

<img src="_imgs/demo.gif">
