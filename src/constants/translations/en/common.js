export default {
    hello: "Hello",
    languageSelector: "Select Your Language",
    nested: {
        introduction: "My name is Marko",
    },
};
