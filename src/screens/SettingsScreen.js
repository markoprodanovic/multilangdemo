import React from "react";
import { Text, View } from "react-native";

import Selector from "../components/LanguageSelector";

export default function SettingsScreen() {
    return (
        <View
            style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
            }}
        >
            <Selector />
        </View>
    );
}
