import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { useTranslation } from "react-i18next";

export default function HomeScreen() {
    const { t } = useTranslation();
    return (
        <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
            <Text style={styles.hello}>{t("common:hello")}</Text>
            <Text style={styles.introduction}>
                {t("common:nested:introduction")}
            </Text>
        </View>
    );
}

const styles = StyleSheet.create({
    hello: { fontSize: 36, fontWeight: "bold" },
    introduction: { fontSize: 22, marginTop: 10 },
});
