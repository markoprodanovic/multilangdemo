import React from "react";
import "./src/constants/IMLocalize";

import RootNavigator from "./src/navigation/RootNavigator";

export default function App() {
    return <RootNavigator />;
}
